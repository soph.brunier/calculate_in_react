import React, { Component } from 'react';
import './App.css';
import InputNumber from './InputNumber';
import Operation from './Operation';
import Button from './Button';
import Result from './Result';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      firstumber: 0,
      secondNumber: 0,
      operation: '+',
      result: 0,
    }
  }

  handleChange(number, event) {
    switch (number) {
      case 'firstNumber':
        this.setState({ firstNumber: event.target.value });
        break;
      case 'secondNumber':
          this.setState({ secondNumber: event.target.value });
          break;
      default:
        break;
    }
  }

  handleOperation = (event) => {
    this.setState({ operation: event.target.value });
  }

  calculate = () => {
    let operationResult = 0;
    switch (this.state.operation) {
      case '+':
        operationResult = Number(this.state.firstNumber) + Number(this.state.secondNumber);
        break;
        case '-':
        operationResult = Number(this.state.firstNumber) - Number(this.state.secondNumber);
        break;
        case 'x':
        operationResult = Number(this.state.firstNumber) * Number(this.state.secondNumber);
        break;
        case '/':
        operationResult = Number(this.state.firstNumber) / Number(this.state.secondNumber);
        break;
      }
    this.setState({result: operationResult});
  }

  randomiseNumbers = () => {
    this.setState({
      firstNumber: Math.floor(Math.random()*100),
      secondNumber: Math.floor(Math.random()*100),
    })
  }

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <div><h3 style={{textDecorationLine: 'underline'}}>Calculator</h3></div>
          <div>
            <div><InputNumber onChange={(event) => {this.handleChange('firstNumber', event)}} number={this.state.firstNumber}/></div>
            <div><Operation onSelect={this.handleOperation} /></div>
            <div><InputNumber onChange={(event) => {this.handleChange('secondNumber', event)}} number={this.state.secondNumber}/></div>
            <Button btnName="Random Numbers" onClick={this.randomiseNumbers} />
            <Button btnName="Calculate" onClick={this.calculate} />
          </div>
          <div>
            <p>  {this.state.firstNumber || 0}</p>
            <p>
              <span style={{fontSize: 25}}>{this.state.operation} </span>
              <span> {this.state.secondNumber || 0}</span></p>
              <hr/>
            <Result result={this.state.result} />
          </div>
        </header>
      </div>
    );
  }
}

export default App;
