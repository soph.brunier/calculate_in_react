import React, { Component } from 'react';

class Operation extends Component {

  render() {
    return (
        <select onChange={this.props.onSelect}>
          <option value='+'>+</option>
          <option value='-'>-</option>
          <option value='x'>x</option>
          <option value='/'>/</option>
        </select>
    )
  }

}

export default Operation;
