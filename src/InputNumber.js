import React, { Component } from 'react';

class InputNumber extends Component {


  render() {
    return (
        <input type='number' onChange={this.props.onChange} value={this.props.number}></input>
    )
  }

}

export default InputNumber;
